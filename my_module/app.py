from flask import Flask, render_template, request, redirect, url_for
import xmlrpc.client

app = Flask(__name__)

# Fonction pour récupérer les données des véhicules depuis Odoo
def get_vehicle_data():
    url = 'http://localhost:8069'
    db = 'admin'
    username = 'neil.dinia@etu.unige.ch'
    password = 'admin'

    common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))

    uid = common.authenticate(db, username, password, {})

    if not uid:
        return None

    models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))

    vehicle_ids = models.execute_kw(db, uid, password, 'my_module.vehicle', 'search', [[]])

    vehicles = models.execute_kw(db, uid, password, 'my_module.vehicle', 'read', [vehicle_ids], {'fields': ['brand', 'model', 'vin', 'year', 'mileage']})

    return vehicles

# Route pour afficher la page HTML avec les données des véhicules
@app.route('/')
def index():
    vehicles = get_vehicle_data()
    return render_template('index.html', vehicles=vehicles)

# Route pour afficher le formulaire d'ajout de véhicule
@app.route('/add_vehicle', methods=['GET'])
def add_vehicle_form():
    return render_template('add_vehicle.html')

# Route pour gérer l'ajout de véhicule
@app.route('/add_vehicle', methods=['POST'])
def add_vehicle():
    url = 'http://localhost:8069'
    db = 'admin'
    username = 'neil.dinia@etu.unige.ch'
    password = 'admin'

    common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))

    uid = common.authenticate(db, username, password, {})

    if not uid:
        return "Erreur d'authentification"

    models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))

    brand = request.form['brand']
    model = request.form['model']
    vin = request.form['vin']
    year = request.form['year']
    mileage = request.form['mileage']

    vehicle_id = models.execute_kw(db, uid, password, 'my_module.vehicle', 'create', [{'brand': brand, 'model': model, 'vin': vin, 'year': year, 'mileage': mileage}])

    if vehicle_id:
        return redirect(url_for('index'))
    else:
        return "Erreur lors de l'ajout du véhicule"

if __name__ == '__main__':
    app.run(debug=True)