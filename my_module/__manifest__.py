# -*- coding: utf-8 -*-
{
    'name': 'my_module',
    'version': '1.0',
    'summary': 'Module de gestion des véhicules pour concessionnaire automobile',
    'author': 'ND',
    'license': 'LGPL-3',
    'website': 'Lien vers votre site web',
    'category': 'Inventory',
    'depends': ['base'],
    'data':[
        'security/ir.model.access.csv',
        'views/menu.xml',
        'views/vehicle_views.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
