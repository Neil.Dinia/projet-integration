from odoo import models, fields # type: ignore

class Vehicle(models.Model):
    _name = 'my_module.vehicle'
    _description = "Vehicle.Records"

    brand = fields.Char(string='Marque', required=True)
    model = fields.Char(string='Modèle', required=True)
    vin = fields.Char(string='N°chassis', required=True)
    year = fields.Char(string='Année', required=True)
    mileage = fields.Char(string='Kilométrage', required=True)