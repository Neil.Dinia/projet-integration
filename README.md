#Description brève du projet

Le projet vise à améliorer la gestion des stocks de véhicules d'une concession automobile. Ce projet inclut la création d'un module Odoo et d'une application web flask. Cette application web permet d'afficher les véhicules dans le stock mais également d'en enregistrer des nouveaux.

#Description des sous-dossiers du repo

Le dossier "security" contient le fichier qui permet de donners les accèss.
Le dossier "models" contient contient un fichier "vehicle.py" dans lequel se trouve le modèle "my_module.vehicle".
Le dossier "static" contient le css pour le formulaire
Le dossier "templates" contient des fichiers html pour afficher les véhicules sur la page web et récupérer les informations du formulaire.
Le dossier "views" contient des fichiers html qui permettent d'avoir les menus et le formulaire sur Odoo.
Le fichier "manifest.py" dans la racine du projet permet d'initer les paramètres du module
Le fichier "app.py" dans la racine du projet correspond à l'application web.

#Procédure d'installation

Le déploiement de la solution en local implique les étapes suivantes :
-	Installez Odoo 17, Flask et les dépendances requises.
-	Vérifier que le serveur Postgres est actif.
-	Insérer le dossier « my_module » dans le dossier « addons » de l’installation Odoo.
-	Dans le fichier « app.py » vérifier que les identifiants sont corrects pour la connexion XML-RPC.
-	Lancer le serveur Odoo.
-	Lancer l’application.
Voici les identifiants que j’ai utilisé :
db = ‘admin’
username = ‘neil.dinia@etu.unige.ch’
password = ‘admin’
